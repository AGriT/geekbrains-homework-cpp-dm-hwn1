#include <iostream>
#include <optional>
#include <string>

using namespace std;

struct stPerson
{
	string m_strName;
	string m_strSurName;
	optional<string> m_strMiddleName;

	bool operator<(const stPerson& stRight)
	{
		return tie(m_strSurName, m_strName, m_strMiddleName) < tie(stRight.m_strSurName, stRight.m_strName, stRight.m_strMiddleName);
	}

	bool operator==(const stPerson& stRight)
	{
		return tie(m_strSurName, m_strName, m_strMiddleName) == tie(stRight.m_strSurName, stRight.m_strName, stRight.m_strMiddleName);
	}


	friend ostream& operator<<(ostream& ostOut, const stPerson& stCurrent) 
	{
		ostOut << stCurrent.m_strSurName << " " << stCurrent.m_strName << " ";

		if(stCurrent.m_strMiddleName)
			ostOut << *stCurrent.m_strMiddleName << " ";

		return ostOut;
	}
};


int main()
{

	return 0;
}